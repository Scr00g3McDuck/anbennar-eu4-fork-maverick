
country_decisions = {

	# black_demense_nation = { #Z99
		# major = yes
		# potential = {
			# always = no	#this is just a debuggy thing so prevent people from getting it
			# has_country_flag = lich_ruler
			# government = theocracy
			# capital_scope = { continent = europe }
			# NOT = { has_country_flag = formed_black_demense_flag }
			
			# normal_or_historical_nations = yes
			# was_never_end_game_tag_trigger = yes
		# }
		# allow = {
			# is_at_war = no
			# is_free_or_tributary_trigger = yes
			# is_nomad = no			
			# NOT = { exists = Z99 }
			
			# num_of_cities = 100
			
			# has_country_flag = lich_ruler
			
		# }
		# effect = {
			# change_tag = Z99
			# hidden_effect = { restore_country_name = yes }
			# swap_non_generic_missions = yes
			# remove_non_electors_emperors_from_empire_effect = yes
			# if = {
				# limit = {
					# NOT = { government_rank = 3 }
				# }
				# set_government_rank = 3
			# }
			
			# if = {
				# limit = {
					# NOT = { government = theocracy }
				# }
				# change_government = theocracy
				# add_government_reform = magocracy_reform	#make it unique later
			# }
			
			# add_country_modifier = {
				# name = "centralization_modifier"
				# duration = 7300
			# }
			# add_prestige = 50
			# if = {
				# limit = {
					# has_custom_ideas = no
				# }
				# country_event = { id = ideagroups.1 } #Swap Ideas
			# }
			# set_country_flag = formed_black_demense_flag
		# }
		# ai_will_do = {
			# factor = 1
		# }
	# }
	
	acolyte_influence = {
		major = yes
		potential = {
			ai = no
			tag = Z99
			calc_true_if = {
				all_subject_country = {
					is_subject_of_type = acolyte_dominion
				}
				amount = 2
			}
		}
		allow = {
			always = yes
		}
		effect = {
			custom_tooltip = acolyte_influence_tooltip
			if = {
				limit = { Z75 = { exists = yes is_subject_of_type = acolyte_dominion } }
				custom_tooltip = Z75_acolyte_influence_tooltip
			}
			if = {
				limit = { Z76 = { exists = yes is_subject_of_type = acolyte_dominion } }
				custom_tooltip = Z76_acolyte_influence_tooltip
			}
			if = {
				limit = { Z77 = { exists = yes is_subject_of_type = acolyte_dominion } }
				custom_tooltip = Z77_acolyte_influence_tooltip
			}
			if = {
				limit = { Z78 = { exists = yes is_subject_of_type = acolyte_dominion } }
				custom_tooltip = Z78_acolyte_influence_tooltip
			}
			if = {
				limit = { Z79 = { exists = yes is_subject_of_type = acolyte_dominion } }
				custom_tooltip = Z79_acolyte_influence_tooltip
			}
			if = {
				limit = { Z80 = { exists = yes is_subject_of_type = acolyte_dominion } }
				custom_tooltip = Z80_acolyte_influence_tooltip
			}
			if = {
				limit = { Z81 = { exists = yes is_subject_of_type = acolyte_dominion } }
				custom_tooltip = Z81_acolyte_influence_tooltip
			}
			custom_tooltip = acolyte_influence_refresh_tooltip
			
			hidden_effect = { country_event = { id = black_demesne.100 } }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	send_the_apostle = {
		major = yes
		potential = {
			OR = {
				tag = Z99
				is_subject_of_type = acolyte_dominion
			}
			NOT = { has_country_flag = debug_menu }
		}
		
		provinces_to_highlight = {
			owned_by = ROOT
			is_territory = yes
			AND = {
				NOT = { is_core = ROOT }
				NOT = { has_construction = core }
			}
		}
		
		allow = {
			adm_power = 50
			any_owned_province = { 
				is_territory = yes
				AND = {
					NOT = { is_core = ROOT }
					NOT = { has_construction = core }
				}
			}
		}
		effect = {
			country_event = { id = black_demesne.3 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	finding_the_surge = {
		major = yes
		potential = {
			tag = Z99
			has_country_flag = bd_1_2
			NOT = { has_global_flag = jorgurem_alive }
		}
		
		provinces_to_highlight = {
			has_province_flag = frozen_surge
		}
		
		allow = {
			mil_power = 20
			any_province = {
				custom_trigger_tooltip = {
					tooltip = frozen_surge_tooltip
					has_province_flag = frozen_surge
				}
				units_in_province = 20
				has_ruler_leader_from = ROOT
			}
		}
		effect = {
			add_mil_power = -20
			country_event = { id = black_demesne.7 }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	subjugating_the_beast = {
		major = yes
		potential = {
			tag = Z99
			has_country_flag = bd_1_3
			NOT = { has_global_flag = jorgurem_is_dead }
		}
		
		provinces_to_highlight = {
			has_province_modifier = jorgurem_the_frozen
		}
		
		allow = {
			mil_power = 20
			any_province = {
				has_province_modifier = jorgurem_the_frozen
				has_ruler_leader_from = ROOT
			}
		}
		effect = {
			add_mil_power = -20
			hidden_effect = {
				set_ruler_flag = npc_jorgurem
				country_event = { id = magic_duel.100 }
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	jorgurem_the_fallen = {
		major = yes
		potential = {
			tag = Z99
			has_country_flag = jorgurem_arise
		}
		
		provinces_to_highlight = {
		}
		
		allow = {
			is_at_war = yes
			mil_power = 10
			NOT = { has_country_flag = black_jorgurem_weakened }
			any_province = {
				has_ruler_leader_from = ROOT
				OR = {
					unit_in_battle = yes
					unit_in_siege = yes
				}
			}
		}
		effect = {
			add_mil_power = -10
			country_event = { id = black_demesne.11 }
			hidden_effect = { set_country_flag = debug_menu }
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
